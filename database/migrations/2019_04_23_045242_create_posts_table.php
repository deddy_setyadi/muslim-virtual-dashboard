<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->string("slug");
            $table->text("description")->nullable();
            $table->string("youtube_url_video");
            $table->string("youtube_id");
            $table->string("link_url")->nullable();
            $table->text("image")->nullable();
            $table->boolean("is_published")->default(0);
            $table->timestamp("published_date")->nullable();
            $table->bigInteger("category_id")->unsigned();
            $table->bigInteger("view_count")->default(0)->unsigned();
            $table->bigInteger("like_count")->default(0)->unsigned();
            $table->bigInteger("dislike_count")->default(0)->unsigned();
            $table->bigInteger("comment_count")->default(0)->unsigned();
            $table->time("video_duration")->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
