<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string("site_title")->nullable();
            $table->text("site_description")->nullable();
            $table->text("site_keywords")->nullable();
            $table->text("site_about")->nullable();
            $table->string("site_contact")->nullable();
            $table->text("site_address")->nullable();
            $table->string("youtube_channel_id")->nullable();
            $table->string("facebook_account")->nullable();
            $table->string("instagram_account")->nullable();
            $table->string("twitter_account")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
