function readUrl(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var img = new Image();

        img.src = window.URL.createObjectURL(input.files[0]);

        img.onload = function () {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            if (width >= 1280 && height >= 720) {
                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $(input).val("");
                $(".img-error").show().delay(3000).fadeOut();
                return false;
            }
        };
    }
}

function chooseFile() {
    $('#file').click();
}
