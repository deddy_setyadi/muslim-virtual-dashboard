function swal_alert_delete(el, custom_msg) {
    custom_msg = custom_msg || "Anda yakin ingin menghapus data ini?";

    msg = custom_msg;

    let target = $(el).attr('href');

    swal({
        title: msg,
        // text: "Once deleted, you will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax(
                    {
                        type: "DELETE",
                        url: target,
                        success: function (data) {
                            swal("Berhasil dihapus", {
                                icon: "success",
                            })
                                .then((value) => {
                                    window.location.reload();
                                });
                        }
                    }
                );
            }
        });
}