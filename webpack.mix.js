const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles(['public/css/themify-icons.css'], 'public/css/themify-icons.min.css')
    .styles(['public/css/classy-nav.css'], 'public/css/classy-nav.min.css')
    .styles(['public/css/animate.css'],'public/css/animate.min.css')
    .styles(['public/css/magnific-popup.css'],'public/css/magnific-popup.min.css')
    .styles(['public/css/style.css'],'public/css/style.min.css')
    .options({
        processCssUrls: false
    })
    .scripts(['public/js/plugins/plugins.js'],'public/js/plugins/plugins.min.js')
    .scripts(['public/js/active.js'],'public/js/active.min.js');