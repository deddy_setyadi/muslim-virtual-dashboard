<!DOCTYPE html>
<html>
<head>
	<title><?= isset($channel) ?  $channel['title'] : "" ?></title>
</head>
<body>
	<h1><a href="<?= isset($channel) ? $channel['link'] : ""?>"><?= $channel['title'] ?></a></h1>
	<ul>
		<?php if ( ! empty($items)) {
            foreach ($items as $item) : ?>
            <li>
                <a href="<?= $item['loc'] ?>"><?= (empty($item['title'])) ? $item['loc'] : $item['title'] ?></a>
                <small>(last updated: <?= date('Y-m-d\TH:i:sP', strtotime($item['lastmod'])) ?>)</small>
            </li>
            <?php endforeach;
        } ?>
	</ul>
</body>
</html>