<div class="row">
    <div class="col-md-12">
        <a href="{!!route($url.".create") !!}" data-original-title="Tambah {!! $title !!}"
           data-toggle="tooltip" class="btn btn-primary">
            <i class="fa fa-plus"></i> Buat Baru
        </a>
    </div>
</div>
<br/>