@extends('layouts.front')

@section('title', "Tentang Kami | ". config("app.name"))

@section('content')

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay lazy"
             style="background-image: url({!! url('img/bg-img/40.jpg') !!});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{!! url('/') !!}">
                                    <i class="fa fa-home" aria-hidden="true"></i>Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                “Tentang Kami”
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <div class="about-us-area">
        <div class="jumbotron-post">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <!-- About Us Content -->
                    <div class="about-us-content bg-white mb-30 p-30 box-shadow">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>MUSLIM VIRTUAL</h5>
                        </div>
                        <p>Muslim Virtual adalah sebuah digital video magazine yang berisi konten edutainment tentang
                            Islam di era virtual.</p>

                        <p>Muslim Virtual merupakan sebuah upaya untuk memberi warna dalam khazanah Islam di era virtual
                            dengan menyuguhkan rubrik-rubrik program: reportase, dokumenter, opini, web series dan ruang
                            interaksi bagi generasi millenial dalam memahami Islam.
                            Sebab di masa sekarang jejak digital tampil mengemuka.</p>

                        <p>
                            Tak sedikit dari kita yang lebih suka mengakses informasi agama secara virtual.
                            Sehingga kami yakin jika dapat aktif menebar karya, energi positif akan mudah menular dan
                            semakin menggelora.
                        </p>

                        <p>
                            Semoga apa yang tersaji di muslimvirual.co dapat mendatangkan manfaat dan keberkahan bagi
                            kita
                            semua.
                        </p>
                        <p>
                            Salam Takdhim,
                        </p>
                        <br/>
                        <p>
                            Sobat Virtual.
                        </p>


                        <div class="section-heading mt-30">
                            <h5>Tim muslim virtual</h5>
                        </div>
                        <div class="single-team-member d-flex align-items-center">
                            <div class="team-member-thumbnail">
                                <img src="{!! asset("img/avatars/avatar2.png") !!}" alt="">
                            </div>
                            <div class="team-member-content">
                                <h6>M. Misbahul Ulum</h6>
                                <span>General Manager</span>
                            </div>
                        </div>
                        <div class="single-team-member d-flex align-items-center">
                            <div class="team-member-thumbnail">
                                <img src="{!! asset("img/avatars/avatar4.png") !!}" alt="">
                            </div>
                            <div class="team-member-content">
                                <h6>Vedy Santoso</h6>
                                <span>Creative Director</span>
                            </div>
                        </div>
                        <div class="single-team-member d-flex align-items-center">
                            <div class="team-member-thumbnail">
                                <img src="{!! asset("img/avatars/avatar1.png") !!}" alt="">
                            </div>
                            <div class="team-member-content">
                                <h6>Khalimatu Nisa</h6>
                                <span>Editor in Chief</span>
                            </div>
                        </div>
                        <div class="single-team-member d-flex align-items-center">
                            <div class="team-member-thumbnail">
                                <img src="{!! asset("img/avatars/avatar3.png") !!}" alt="">
                            </div>
                            <div class="team-member-content">
                                <h6> Deddy Setyadi</h6>
                                <span>Webmaster</span>
                            </div>
                        </div>
                    </div>
                </div>


                @include("frontend.include._right_sidebar")
            </div>
        </div>
    </div>
@stop