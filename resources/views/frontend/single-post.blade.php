@extends('layouts.front')

@section('title', $post->title ."|". config("app.name"))

@section('content')
    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{!! url("/") !!}">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    Home
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{!! url("category/". $post->category->slug) !!}"> {!! $post->category->name !!}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{!! $post->title !!}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-details-area" style="max-width: 100%;">
        <div class="jumbotron-post">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="single-video-area bg-white mb-30 box-shadow">
                        <iframe src="https://www.youtube.com/embed/{!! $post->youtube_id !!}"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                frameborder="0"
                                allowfullscreen
                                style="width:100%;height:100%;"></iframe>
                        <!-- Video Meta Data -->
                        <div class="video-meta-data d-flex align-items-center justify-content-between">
                            <h6 class="total-views">{!! format_nominal($post->view_count) !!} Dilihat</h6>
                            <div class="like-dislike d-flex align-items-center">
                                <button type="button">
                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    {!! format_nominal($post->like_count) !!} Menyukai
                                </button>
                                <button type="button">
                                    <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                    {!! format_nominal($post->dislike_count) !!} Tidak Suka
                                </button>
                                <p>
                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    {!! format_nominal($post->comment_count) !!} Komentar
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <div class="post-meta">
                                <a href="#">{!! date_frontend($post->published_date) !!}</a>
                                <a href="{!! url("category/".$post->category->slug) !!}">{!! $post->category->name !!}</a>
                            </div>
                            <h4 class="post-title">{!! $post->title !!}</h4>

                            <p>{!! $post->description !!}</p>
                            @if(count($post->tagNames()) > 0 )
                                @foreach($post->tagged as $tag)
                                    <a href="{!! url("tag/".$tag->tag_slug) !!}" class="badge badge-secondary">
                                        {!! $tag->tag_name !!}
                                    </a>
                            @endforeach
                        @endif
                        <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="{!! $post->getShareUrl('facebook')!!}" target="_blank" class="facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                    Bagikan di Facebook
                                </a>
                                <a href="{!! $post->getShareUrl('twitter')!!}" target="_blank" class="twitter">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                    Bagikan di Twitter
                                </a>
                                <a href="{!! $post->getShareUrl('whatsapp')!!}" target="_blank" class="bg-green">
                                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                    Bagikan di Whatsapp
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- Related Post Area -->
                    @if($related_post->count() > 0)
                        <div class="related-post-area bg-white mb-30 px-30 pt-30 box-shadow">
                            <!-- Section Title -->
                            <div class="section-heading">
                                <h5>Video Terkait</h5>
                            </div>

                            <div class="row">
                                <!-- Single Blog Post -->
                                @foreach($related_post as $video)
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="single-blog-post style-4 mb-30">
                                            @include("frontend.include._post_list")
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                <!-- Sidebar Widget -->
                @include("frontend.include._right_sidebar")
            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->
@stop

@section("js")
    <script src="{{ asset('js/share.js') }}"></script>
@endsection