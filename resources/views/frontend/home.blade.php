@extends('layouts.front')

@section('title', config("app.name"))

@section('content')
    @include("frontend.include._hero")

    {{--    @include("frontend.include._left_sidebar")--}}

    <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
    {{--    @include("frontend.include._latest_video")--}}
    <!-- Category Posts Area -->
        <div class="trending-now-posts mb-30">
            <!-- Section Title -->
            <div class="section-heading">
                <h5>RUBRIK</h5>
            </div>

            <div class="trending-post-slides owl-carousel">

                @foreach($categories as $category)
                    <div class="single-trending-post">
                        <img class="lazy" src="{!! take_image($category->image,'standard') !!}" alt="">
                        <div class="post-content">
                            <a href="{!! url("category/".$category->slug) !!}" class="post-cata">
                                {!! $category->name !!}
                            </a>
                            <a href="{!! url("category/".$category->slug) !!}" class="post-title">
                                {!! $category->description !!}
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

        <!-- Feature Video Posts Area -->
        <div class="feature-video-posts mb-30">
            <!-- Section Title -->
            <div class="section-heading">
                <h5>VIDEO OLEH MUSLIM VIRTUAL</h5>
            </div>

            <div class="featured-video-posts">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <!-- Single Featured Post -->
                        <div class="single-featured-post">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <img class="lazy" src="{!! take_image($first_featured->image, 'standard') !!}" alt="">
                                <a href="{!! url("/show/". $first_featured->slug) !!}" class="video-play"><i
                                            class="fa fa-play"></i></a>
                            </div>
                            <!-- Post Contetnt -->
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#">{!! date_frontend($first_featured->published_date) !!}</a>
                                    <a href="{!! url("/category/". $first_featured->category->slug) !!}">
                                        {!! $first_featured->category->name !!}
                                    </a>
                                </div>
                                <a href="{!! url("/show/". $first_featured->slug) !!}" class="post-title">
                                    {!! $first_featured->title !!}
                                </a>
                                <p>{!! \Str::limit($first_featured->description,200) !!}</p>
                            </div>
                            <!-- Post Share Area -->
                            <div class="post-share-area d-flex align-items-center justify-content-between">
                                <!-- Post Meta -->
                                <div class="post-meta pl-3">
                                    <a href="#"><i class="fa fa-eye"
                                                   aria-hidden="true"></i> {!! format_nominal($first_featured->view_count) !!}
                                    </a>
                                    <a href="#"><i class="fa fa-thumbs-o-up"
                                                   aria-hidden="true"></i> {!! format_nominal($first_featured->like_count) !!}
                                    </a>
                                    <a href="#"><i class="fa fa-comments-o"
                                                   aria-hidden="true"></i> {!! format_nominal($first_featured->comment_count) !!}
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <!-- Featured Video Posts Slide -->
                        <div class="featured-video-posts-slide owl-carousel">
                            <div class="single--slide">
                                <!-- Single Blog Post -->
                                @foreach($first_list_featured as $video)
                                    <div class="single-blog-post d-flex style-3">
                                        @include("frontend.include._post_list",["icon_play" => false, "imgSize" => 'default'])
                                    </div>
                                @endforeach

                            </div>
                            @if($second_list_featured->count() > 0)
                                <div class="single--slide">
                                    <!-- Single Blog Post -->
                                    @foreach($second_list_featured as $video)
                                        <div class="single-blog-post d-flex style-3">
                                            @include("frontend.include._post_list",["icon_play" => false, "imgSize" => 'default'])
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                {{--Most viewed video--}}
                <div class="most-viewed-videos mb-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>Paling Banyak Dilihat</h5>
                    </div>

                    <div class="most-viewed-videos-slide owl-carousel">
                        @foreach($most_viewed as $video)
                            <div class="single-blog-post style-4">
                                @include("frontend.include._post_list", ["video" => $video])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                {{--Most viewed video--}}
                <div class="most-viewed-videos mb-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>Paling Disukai</h5>
                    </div>

                    <div class="most-viewed-videos-slide owl-carousel">
                        @foreach($most_liked as $video)
                            <div class="single-blog-post style-4">
                                @include("frontend.include._post_list", ["video" => $video])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    @include("frontend.include._right_sidebar")--}}
@stop