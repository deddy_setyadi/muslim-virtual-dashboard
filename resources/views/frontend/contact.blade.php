@extends('layouts.front')

@section('title', "Kontak | ". config("app.name"))

@section('content')

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay lazy"
             style="background-image: url({!! url('img/bg-img/40.jpg') !!});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Kontak</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{!! url('/') !!}">
                                    <i class="fa fa-home" aria-hidden="true"></i>Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                “Kontak”
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <div class="contact-area">
        <div class="jumbotron-post">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <!-- About Us Content -->
                    <div class="contact-content-area bg-white mb-30 p-30 box-shadow">
                        <div class="map-area mb-30">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.4163902093655!2d110.37993851448527!3d-7.745581494417328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59328eb654eb%3A0x7d8bde9c5f7000bd!2sMuslim+Virtual!5e0!3m2!1sen!2sid!4v1557925317568!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>

                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>KONTAK MUSLIM VIRTUAL</h5>
                        </div>

                        <div class="contact-information mb-30">
                            <!-- Single Contact Info -->
                            <div class="single-contact-info d-flex align-items-center">
                                <div class="icon mr-15">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="text">
                                    <h6>{!! config("app.contact.address") !!}</h6>
                                </div>
                            </div>

                            <!-- Single Contact Info -->
                            <div class="single-contact-info d-flex align-items-center">
                                <div class="icon mr-15">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <div class="text">
                                    <h6>{!! config("app.contact.email") !!}</h6>
                                </div>
                            </div>
                            <div class="single-contact-info d-flex align-items-center">
                                <div class="icon mr-15">
                                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                </div>
                                <div class="text">
                                    <h6>{!! config("app.contact.phone") !!}</h6>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                @include("frontend.include._right_sidebar")
            </div>
        </div>
    </div>
@stop