<div class="single-catagory-post d-flex flex-wrap">
    <!-- Thumbnail -->
    <div class="post-thumbnail bg-img lazy"
         style="background-image: url({!! take_image($post->image,'high') !!});">
        <a href="{!! url("show/".$post->slug) !!}" class="video-play"><i class="fa fa-play"></i></a>
    </div>

    <!-- Post Contetnt -->
    <div class="post-content">
        <div class="post-meta">
            <a href="#">{!! date_frontend($post->published_date) !!}</a>
            <a href="{!! url("category/".$post->category->slug) !!}">{!! $post->category->name !!}</a>
        </div>
        <a href="{!! url("show/".$post->slug) !!}" class="post-title">{!! $post->title !!}</a>
        <!-- Post Meta -->
        <div class="post-meta-2">
            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> {!! numberformat_kilo($post->view_count) !!}</a>
            <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> {!! numberformat_kilo($post->like_count) !!}</a>
            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> {!! numberformat_kilo($post->comment_count) !!}</a>
        </div>
        <p>{!! Str::limit($post->description,250) !!}</p>
    </div>
</div>