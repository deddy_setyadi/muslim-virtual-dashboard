<!-- ##### Header Area Start ##### -->
<header class="header-area">

    <!-- Navbar Area -->
    <div class="mag-main-menu">
        <div class="classy-nav-container breakpoint-off">
            <!-- Menu -->
            <nav class="classy-navbar justify-content-between" id="magNav">
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Nav Content -->
                <div class="nav-content d-flex align-items-center">
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="#"><b>Rubrik</b></a>
                                    <ul class="dropdown">
                                        @foreach($categories as $category)
                                            <li>
                                                <a href="{!! url("/category/".$category->slug) !!}">
                                                    {!! $category->name !!}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- Nav End -->
                    </div>

                </div>
                <!-- Nav brand -->
                <a href="{!! url("/") !!}" class="nav-brand">
                    <img src="{!! asset("img/logo/logo-only.png") !!}" style="max-height: 36px" alt=""/> <span
                            class="text-brand">MUSLIM VIRTUAL</span>
                </a>

                <div class="nav-content d-flex align-items-center">
                    <div class="top-meta-data d-flex align-items-center">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="{!! url("search") !!}" method="GET" autocomplete="off">
                                <input type="search" name="search" id="topSearch" autocomplete="off"
                                       placeholder="Search..." required="required">
                                <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- ##### Header Area End ##### -->
