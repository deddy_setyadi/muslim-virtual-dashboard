<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <div class="container">
        <div class="row">
            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="footer-widget">
                    <!-- Logo -->
                    <a href="{!! url("/") !!}" class="foo-logo">
                        <img src="{!! asset("img/logo/logo-only.png") !!}" alt=""
                             style="max-height: 40px;width: auto;margin-top:-10px">
                        <p style="display: inline; font-size: 30px;font-weight: 700; color: #3E6864">MUSLIM VIRTUAL</p>
                    </a>
                    <p>{!! config("app.description") !!}</p>
                    <div class="footer-social-info">
                        <a href="{!! config("app.social.facebook") !!}" class="facebook" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="{!! config("app.social.youtube") !!}" class="google-plus" target="_blank">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                        <a href="{!! config("app.social.instagram") !!}" class="instagram" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="footer-widget">
                    <h6 class="widget-title">RUBRIK</h6>
                    <nav class="footer-widget-nav">
                        <ul>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{!! url("category/".$category->slug) !!}">
                                        <i class="fa fa-angle-double-right"
                                           aria-hidden="true"></i> {!! $category->name !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="footer-widget">
                    <h6 class="widget-title">Kontak</h6>
                    <ul>
                        <li>
                            <p class="text-muted"><i class="fa fa-home"></i> {!! config("app.contact.address") !!}</p>
                        </li>
                        <li>
                            <p class="text-muted"><i class="fa fa-envelope-o"></i> {!! config("app.contact.email") !!}
                            </p>
                        </li>
                        <li>
                            <p class="text-muted"><i class="fa fa-whatsapp"></i> {!! config("app.contact.phone") !!}
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="text-muted">
                        Copyright &copy; {!! date('Y') !!} Muslim Virtual
                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <nav class="footer-nav">
                        <ul>
                            <li><a href="{!! url("/") !!}">Home</a></li>
                            <li><a href="{!! url("about-us") !!}">Tentang Kami</a></li>
                            <li><a href="{!! url("media-guide") !!}">Pedoman</a></li>
                            <li><a href="{!! url("contact") !!}">Kontak</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->
