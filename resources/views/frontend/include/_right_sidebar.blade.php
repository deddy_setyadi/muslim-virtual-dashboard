<div class="col-12 col-md-6 col-lg-5 col-xl-4">
    <div class="sidebar-area bg-white mb-30 box-shadow">

        <!-- Sidebar Widget -->
{{--        <div class="single-sidebar-widget p-30">--}}
{{--            <!-- Section Title -->--}}
{{--            <div class="section-heading">--}}
{{--                <h5>KATEGORI</h5>--}}
{{--            </div>--}}

{{--            <!-- Catagory Widget -->--}}
{{--            <ul class="catagory-widgets">--}}
{{--                @foreach($categories as $category)--}}
{{--                    <li>--}}
{{--                        <a href="{!! url("category/".$category->slug) !!}">--}}
{{--                            <span><i class="fa fa-angle-double-right" aria-hidden="true"></i> {!! $category->name !!}</span>--}}
{{--                            <span>{!! count_video_in_category($category->id) !!}</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}

        <!-- Sidebar Widget -->
        {{--        <div class="single-sidebar-widget">--}}
        {{--            <a href="#" class="add-img"><img src="img/bg-img/add2.png" alt=""></a>--}}
        {{--        </div>--}}

        <div class="single-sidebar-widget p-30">
            <!-- Section Title -->
            <div class="section-heading">
                <h5>Paling disukai</h5>
            </div>
            @foreach($most_liked as $video)
                <div class="single-blog-post d-flex">
                    <div class="post-thumbnail">
                        <img src="{!! take_image($video->image,"default") !!}" alt="">
                    </div>
                    <div class="post-content">
                        <a href="{!! url("show/".$video->slug) !!}" class="post-title">{!! $video->title !!}</a>
                        <div class="post-meta d-flex justify-content-between">
                            <a href="#"><i class="fa fa-eye"
                                           aria-hidden="true"></i> {!! numberformat_kilo($video->view_count) !!}</a>
                            <a href="#"><i class="fa fa-thumbs-o-up"
                                           aria-hidden="true"></i> {!! numberformat_kilo($video->like_count) !!}</a>
                            <a href="#"><i class="fa fa-comments-o"
                                           aria-hidden="true"></i> {!! numberformat_kilo($video->comment_count) !!}</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>