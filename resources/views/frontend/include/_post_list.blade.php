@php
    $icon = isset($icon_play) ? false : true ;
    $imgSize = isset($imgSize) ? $imgSize : "medium";
@endphp

<div class="post-thumbnail">
    <img class="lazy" src="{!! take_image($video->image,$imgSize) !!}" alt="">
    @if($icon)
        <a href="{!! url("/show/". $video->slug) !!}" class="video-play">
            <i class="fa fa-play"></i>
        </a>
    @endif
</div>
<div class="post-content">
    <a href="{!! url("/show/". $video->slug) !!}" class="post-title">
        {!! $video->title !!}
    </a>
    <div class="post-meta d-flex">
        <a href="#">
            <i class="fa fa-eye" aria-hidden="true"></i>
            {!! numberformat_kilo($video->view_count) !!}
        </a>
        <a href="#">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
            {!! numberformat_kilo($video->like_count) !!}
        </a>
        <a href="#">
            <i class="fa fa-comments-o" aria-hidden="true"></i>
            {!! numberformat_kilo($video->comment_count) !!}
        </a>
    </div>
</div>