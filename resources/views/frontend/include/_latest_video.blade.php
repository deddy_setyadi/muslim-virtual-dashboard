<!-- Sports Videos -->
<div class="sports-videos-area">
    <!-- Section Title -->
    <div class="section-heading">
        <h5>VIDEO TERBARU</h5>
    </div>

    <div class="sports-videos-slides owl-carousel mb-30">
        <!-- Single Featured Post -->
        <div class="single-featured-post">
            <!-- Thumbnail -->
            <div class="post-thumbnail mb-50">
                <img src="img/bg-img/22.jpg" alt="">
                <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
            </div>
            <!-- Post Contetnt -->
            <div class="post-content">
                <div class="post-meta">
                    <a href="#">MAY 8, 2018</a>
                    <a href="archive.html">lifestyle</a>
                </div>
                <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From
                    Lowe’s</a>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                    anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                    accusantium doloremque laudantium</p>
            </div>
            <!-- Post Share Area -->
            <div class="post-share-area d-flex align-items-center justify-content-between">
                <!-- Post Meta -->
                <div class="post-meta pl-3">
                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                    <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                    <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                </div>
                <!-- Share Info -->
                <div class="share-info">
                    <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                    <!-- All Share Buttons -->
                    <div class="all-share-btn d-flex">
                        <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Featured Post -->
        <div class="single-featured-post">
            <!-- Thumbnail -->
            <div class="post-thumbnail mb-50">
                <img src="img/bg-img/22.jpg" alt="">
                <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
            </div>
            <!-- Post Contetnt -->
            <div class="post-content">
                <div class="post-meta">
                    <a href="#">MAY 8, 2018</a>
                    <a href="archive.html">lifestyle</a>
                </div>
                <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From
                    Lowe’s</a>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                    anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                    accusantium doloremque laudantium</p>
            </div>
            <!-- Post Share Area -->
            <div class="post-share-area d-flex align-items-center justify-content-between">
                <!-- Post Meta -->
                <div class="post-meta pl-3">
                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                    <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                    <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                </div>
                <!-- Share Info -->
                <div class="share-info">
                    <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                    <!-- All Share Buttons -->
                    <div class="all-share-btn d-flex">
                        <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Featured Post -->
        <div class="single-featured-post">
            <!-- Thumbnail -->
            <div class="post-thumbnail mb-50">
                <img src="img/bg-img/22.jpg" alt="">
                <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
            </div>
            <!-- Post Contetnt -->
            <div class="post-content">
                <div class="post-meta">
                    <a href="#">MAY 8, 2018</a>
                    <a href="archive.html">lifestyle</a>
                </div>
                <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From
                    Lowe’s</a>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                    anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                    accusantium doloremque laudantium</p>
            </div>
            <!-- Post Share Area -->
            <div class="post-share-area d-flex align-items-center justify-content-between">
                <!-- Post Meta -->
                <div class="post-meta pl-3">
                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                    <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                    <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                </div>
                <!-- Share Info -->
                <div class="share-info">
                    <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                    <!-- All Share Buttons -->
                    <div class="all-share-btn d-flex">
                        <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Single Blog Post -->
        <div class="col-12 col-lg-6">
            <div class="single-blog-post d-flex style-3 mb-30">
                <div class="post-thumbnail">
                    <img src="img/bg-img/31.jpg" alt="">
                </div>
                <div class="post-content">
                    <a href="single-post.html" class="post-title">From Wetlands To Canals And Dams Amsterdam Is
                        Alive</a>
                    <div class="post-meta d-flex">
                        <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                        <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                        <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="col-12 col-lg-6">
            <div class="single-blog-post d-flex style-3 mb-30">
                <div class="post-thumbnail">
                    <img src="img/bg-img/32.jpg" alt="">
                </div>
                <div class="post-content">
                    <a href="single-post.html" class="post-title">From Wetlands To Canals And Dams Amsterdam Is
                        Alive</a>
                    <div class="post-meta d-flex">
                        <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                        <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                        <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="col-12 col-lg-6">
            <div class="single-blog-post d-flex style-3 mb-30">
                <div class="post-thumbnail">
                    <img src="img/bg-img/33.jpg" alt="">
                </div>
                <div class="post-content">
                    <a href="single-post.html" class="post-title">From Wetlands To Canals And Dams Amsterdam Is
                        Alive</a>
                    <div class="post-meta d-flex">
                        <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                        <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                        <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="col-12 col-lg-6">
            <div class="single-blog-post d-flex style-3 mb-30">
                <div class="post-thumbnail">
                    <img src="img/bg-img/34.jpg" alt="">
                </div>
                <div class="post-content">
                    <a href="single-post.html" class="post-title">From Wetlands To Canals And Dams Amsterdam Is
                        Alive</a>
                    <div class="post-meta d-flex">
                        <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                        <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                        <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>