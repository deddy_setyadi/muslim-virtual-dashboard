<!-- ##### Hero Area Start ##### -->
<div class="hero-area owl-carousel">
    @foreach($slides as $slide)
        <div class="hero-blog-post bg-img bg-overlay lazy"
             style="background-image: url({!! take_image($slide->image,'maxres') !!});">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="#">{!! date_frontend($slide->published_date) !!}</a>
                                <a href="{!! url("category/".$slide->category->slug) !!}">
                                    {!! $slide->category->name !!}
                                </a>
                            </div>
                            <a href="{!! url("show/".$slide->slug) !!}" class="post-title" data-animation="fadeInUp"
                               data-delay="300ms">
                                {!! $slide->title !!}
                            </a>
                            <a href="{!! url("show/".$slide->slug) !!}" class="video-play" data-animation="bounceIn"
                               data-delay="500ms">
                                <i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- ##### Hero Area End ##### -->