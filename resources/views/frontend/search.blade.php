@extends('layouts.front')

@section('title', "Pencarian $keyword | ". config("app.name"))

@section('content')
    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{!! url('/') !!}">
                                    <i class="fa fa-home" aria-hidden="true"></i>Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Pencarian</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                “{!! $keyword !!}”
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <div class="archive-post-area">
        <div class="jumbotron-post">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <div class="archive-posts-area bg-white p-30 mb-30 box-shadow">
                    @if($posts->count() > 0)
                        @foreach($posts as $post)
                            @include("frontend.include._post_list_category")
                        @endforeach
                        <!-- Pagination -->
                            {{ $posts->appends(["search" => request()->get("search")])->links() }}
                        @else
                            <h4 class="text-center">Tidak terdapat video dengan kata kunci "{!! $keyword!!}"</h4>
                        @endif

                    </div>
                </div>
                @include("frontend.include._right_sidebar")
            </div>
        </div>
    </div>
@stop