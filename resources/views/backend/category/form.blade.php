@extends('adminlte::page')

@section('title', $title)

@section("css")
    <link href="{!! asset('vendor/adminlte/plugins/iCheck/square/blue.css') !!} " rel="stylesheet" type="text/css"/>
@endsection

@section('content_header')
    <h1> {!! request()->segment(4) == "edit" ? "Edit" : "Buat Baru "!!} {!! $title !!}</h1>
@stop

@section('content')
    {!! form_start($form) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            {!! form_row($form->name) !!}
                        </div>
                        <div class="col-md-12">
                            {!! form_row($form->description) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            {!! form_label($form->image) !!}
                            <p class="text-warning text-center text-bold"><i class="fa fa-warning"></i> Ukuran minimal : 1280x720</p>
                            {!! form_widget($form->image, ['attr' => ['style' => 'display:none']]) !!}
                            <div class="text-danger">{!! $errors->first('image') !!}</div>
                            <p class="text-center">
                                <img id="preview_img" class="img img-thumbnail"
                                     style="max-height:200px"
                                     src="{!! @take_image($row->image)!!}"/>
                            </p>
                            <p class="text-danger text-bold text-center img-error" style="display: none;"><i class="fa fa-times"></i>Gambar belum memenuhi ukuran minimal.</p>
                            <p class="text-center">
                                {!! form_row($form->upload) !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <hr/>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2 col-md-offset-10">
                            {!! form_row($form->submit) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('js')
    <script type="text/javascript" src="{!! asset('js/image-upload.js') !!}"></script>
@endpush