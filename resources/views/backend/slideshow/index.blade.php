@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{!! $title !!}</h1>
@stop

@section('content')
    @include('flash::message')
    @include('adminlte::partials.create_button')

    {!! $dataTable->table(["class"=>"table table-bordered table-hover"]) !!}
@endsection

@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
    <script src="{!! asset('js/swal_alert_delete.js') !!} " type="text/javascript"></script>

    {!! $dataTable->scripts() !!}
@endpush