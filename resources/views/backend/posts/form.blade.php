@extends('adminlte::page')

@section('title', $title)

@section("css")
    <link href="{!! asset("vendor/adminlte/plugins/selectize/selectize.bootstrap3.css") !!}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content_header')
    <h1> {!! request()->segment(4) == "edit" ? "Edit" : "Buat Baru "!!} {!! $title !!}</h1>
@stop

@section('content')
    {!! form_start($form) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            {!! form_row($form->youtube_url_video) !!}
                            <p class="text-info processing-video" id="loading-video" style="display: none">
                                <i class="fa fa-spinner fa-spin"></i> Sedang mengambil video dari youtube ...
                            </p>
                            <p class="text-success processing-video" id="success-video" style="display: none">
                                <i class="fa fa-check"></i> Berhasil mengambil video
                            </p>
                            <p class="text-danger processing-video" id="error-video" style="display: none">
                                <i class="fa fa-times"></i> <span id="error-video-text"></span>
                            </p>
                        </div>
                        <div class="col-md-12">
                            {!! form_row($form->title) !!}
                        </div>
                        <div class="col-md-12">
                            {!! form_row($form->category_id) !!}
                        </div>
                        <div class="col-md-12">
                            {!! form_row($form->input_tag) !!}
                        </div>
                        <div class="col-md-12">
                            {!! form_row($form->description) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="video-preview" class="control-label">Preview Video</label>
                            <iframe id="preview_video" style="width: 100%; height: 250px;"
                                    src="https://www.youtube.com/embed/{!! $default_video !!}?autoplay=0&controls=0&modestbranding=1"
                                    frameborder="0" allowfullscreen
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                        </div>
                        <div class="col-md-12">
                            <hr/>
                        </div>
                        <div class="col-md-12">
                            {!! form_label($form->image) !!}
                            <p class="text-warning text-center text-bold">
                                <i class="fa fa-warning"></i> Ukuran minimal : 1280x720
                            </p>
                            {!! form_widget($form->image, ['attr' => ['style' => 'display:none']]) !!}
                            <div class="text-danger">{!! $errors->first('image') !!}</div>
                            <p class="text-center">
                                <img id="preview_img" class="img img-thumbnail"
                                     style="max-height:200px"
                                     src="{!! @take_image($row->image)!!}"/>
                            </p>
                            <p class="text-danger text-bold text-center img-error" style="display: none;"><i
                                        class="fa fa-times"></i>Gambar belum memenuhi ukuran minimal.</p>
                            <p class="text-center">
                                {!! form_row($form->upload) !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <hr/>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2 col-md-offset-10">
                            {!! form_row($form->submit) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('js')
    <script type="text/javascript" src="{!! asset('js/image-upload.js') !!}"></script>
    <script type="text/javascript" src="{!! asset("vendor/adminlte/plugins/selectize/selectize.min.js") !!}"></script>
    <script type="text/javascript">
        $(function () {
            $("#title").change(function () {
                $('.processing-video').hide();
                $("#loading-video").show();
                $("#upload-file").hide();

                if (this.value !== '' && isValidUrl(this.value) === 1) {
                    $.ajax({
                        url: "{!! url("admin/videos/fetch-youtube") !!}",
                        method: "POST",
                        data: {"url": this.value},
                        success: function (data) {
                            $("#loading-video").hide();
                            $("#success-video").show().delay(5000).fadeOut();

                            $("#preview_video").attr("src", "https://youtube.com/embed/" + data.id + "?autoplay=0&showinfo=0&controls=0");
                            $("input[name='title']").val("").val(data.snippet.title);
                            $("input[name='youtube_response']").val("").val(JSON.stringify(data));
                            setTags(data.snippet.tags);
                            $("textarea#description").val("").val(data.snippet.description);

                            let thumbnail = data.snippet.thumbnails;
                            if (typeof (thumbnail.standard) != "undefined" && thumbnail.standard !== null
                                && typeof (thumbnail.maxres) != "undefined" && thumbnail.maxres !== null
                            ) {
                                $("#preview_img").attr("src", data.snippet.thumbnails.standard.url);
                            } else {
                                $("#upload-file").show();
                            }
                        },
                        error: function (data) {
                            $("#loading-video").hide();
                            $("#error-video-text").text(data.responseJSON);
                            $("#error-video").show().delay(5000).fadeOut();
                        }
                    });
                } else {
                    $("#loading-video").hide();
                    $("#error-video-text").text('Video url tidak valid');
                    $("#error-video").show().delay(5000).fadeOut();
                }

            });

            $(".select2").select2({
                width: '100%',
            });

            var tags = [
                    @foreach ($tags as $tag)
                {
                    tag: "{!!  $tag !!}"
                },
                @endforeach
            ];

            $('#tag').selectize({
                delimiter: ',',
                persist: false,
                valueField: 'tag',
                labelField: 'tag',
                searchField: 'tag',
                options: tags,
                create: function (input) {
                    return {
                        tag: input
                    }
                }
            });
        });

        function setTags(tags) {
            let $select = $("#tag")[0].selectize;
            $select.clear();
            $.each(tags, function (key, item) {
                let tag = item.replace(/[^a-zA-Z0-9 -]/, '');
                $select.addOption({"tag": tag});
                $select.addItem(tag);
            });
        }

        function isValidUrl(url) {
            if (/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url)) {
                return 1;
            } else {
                return 0;
            }
        }

    </script>
@endpush