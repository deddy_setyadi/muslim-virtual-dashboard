@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{!! $title !!}</h1>
@stop

@section('content')
    @include('flash::message')
    @include('adminlte::partials.create_button')

    {!! $dataTable->table(["class"=>"table table-bordered table-hover"]) !!}

@endsection

@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
    <script src="{!! asset("vendor/adminlte/plugins/bootbox/all.min.js") !!}" type="text/javascript"></script>
    <script src="{!! asset('js/swal_alert_delete.js') !!} " type="text/javascript"></script>

    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        function optimizeImage(image) {
            var dialog = bootbox.dialog({
                title: 'Processing, please wait ...',
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
            });

            $.ajax({
                url: "{!! url("admin/videos/optimize-image") !!}",
                method: "POST",
                data: {"image": image},
                success: function () {
                    dialog.init(function () {
                        setTimeout(function () {
                            dialog.find('.bootbox-body').html('Sukses');
                        }, 3000);
                    });
                },
                error: function () {
                    dialog.init(function () {
                        setTimeout(function () {
                            dialog.find('.bootbox-body').html('Error');
                        }, 3000);
                    });
                }
            });
        }
    </script>
@endpush