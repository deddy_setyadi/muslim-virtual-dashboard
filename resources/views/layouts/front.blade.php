<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {!! SEO::generate(true) !!}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-site-verification" content="SNam7J3CBcWtPhT_115sY0ZerobUEQnF7PBULIFBUHc"/>
    @include("adminlte::partials.favicon")

<!-- Stylesheet -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="{!! asset("css/google-fonts.css") !!}">
    <link rel="preload" href="{!! asset("fonts/fontawesome-webfont.woff2?v=4.7.0") !!}" as="font"
          crossorigin="anonymous">
    <link rel="preload" href="{!! asset("/fonts/themify.woff?-fvbane") !!}" as="font" crossorigin="anonymous">
    <link rel="preload" href="{!! asset("/fonts/classy.ttf?fftrrv") !!}" as="font" crossorigin="anonymous">

    <link href="{!! asset("/css/bootstrap.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/font-awesome.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/owl.carousel.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/classy-nav.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/themify-icons.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/animate.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/magnific-popup.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    <link href="{!! asset("/css/style.min.css") !!}" rel="prefetch" as="style" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{!! asset("/css/bootstrap.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/font-awesome.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/owl.carousel.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/classy-nav.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/themify-icons.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/animate.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/magnific-popup.min.css") !!}">
    <link rel="stylesheet" href="{!! asset("/css/style.min.css") !!}">

    <!-- ##### All Javascript Script ##### -->
    <link rel="preload" href="{!! asset("/js/jquery/jquery-2.2.4.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/bootstrap/popper.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/bootstrap/bootstrap.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/plugins/lazy.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/plugins/lazy.plugins.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/plugins/plugins.min.js") !!}" as="script">
    <link rel="preload" href="{!! asset("js/active.min.js") !!}" as="script">


@if(isset($analyticCode))
        @if(!is_null($analyticCode) && !empty($analyticCode))
        <!-- Global site tag (gtag.js) - Google Analytics -->

            <script async src="https://www.googletagmanager.com/gtag/js?id={!! $analyticCode !!}"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());
                gtag('config', '{!! $analyticCode !!}');
            </script>
        @endif
    @endif
</head>

<body>
<!-- Preloader -->
<div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>

@include("frontend.include._navbar")

<section class="mag-posts-area d-flex flex-wrap">
    @yield('content')
</section>
@include("frontend.include._footer")

<!-- ##### All Javascript Script ##### -->
<script defer src="{!! asset("/js/jquery/jquery-2.2.4.min.js") !!}"></script>
<script defer src="{!! asset("js/bootstrap/popper.min.js") !!}"></script>
<script defer src="{!! asset("js/bootstrap/bootstrap.min.js") !!}"></script>
<script defer src="{!! asset("js/plugins/lazy.min.js") !!}"></script>
<script defer src="{!! asset("js/plugins/lazy.plugins.min.js") !!}"></script>
<script defer src="{!! asset("js/plugins/plugins.min.js") !!}"></script>
<script defer src="{!! asset("js/active.min.js") !!}"></script>
</body>

</html>