<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CategoryForm extends Form
{

    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                "required" => true,
                "attr"     => [
                    "id"           => "title",
                    "maxlength"    => "255",
                    "autocomplete" => "off"
                ],
                "label"    => "Nama Rubrik",
            ])
            ->add('description', 'textarea', [
                "required" => true,
                "attr"     => [
                    "id"        => "description",
                    "maxlength" => "255",
                    "rows"      => 3
                ],
                "label"    => "Deskripsi",
            ])
            ->add('image', 'file', [
                'attr'     => [
                    'id'       => 'file',
                    'onchange' => 'readUrl(this)',
                    'accept'   => "image/x-png,image/jpg,image/jpeg"
                ],
                'label'    => 'Gambar',
                'required' => true
            ])
            ->add('upload', 'button', [
                'label' => '<i class="fa fa-upload"></i> Browse',
                'attr'  => [
                    'class'   => 'btn btn-flat bg-gray',
                    'onclick' => 'chooseFile()'
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<i class="fa fa-file"></i> Simpan',
                "attr"  => [
                    "id"    => "submit",
                    "class" => "btn btn-flat btn-primary btn-block"
                ]
            ]);
    }
}
