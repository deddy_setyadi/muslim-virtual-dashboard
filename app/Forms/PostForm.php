<?php

namespace App\Forms;

use App\Models\Categories;
use Kris\LaravelFormBuilder\Form;

class PostForm extends Form
{

    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                "required" => true,
                "attr"     => [
                    "id"           => "title",
                    "maxlength"    => "255",
                    "autocomplete" => "off"
                ],
                "label"    => "Judul",
            ])
            ->add('youtube_url_video', 'text', [
                "required" => true,
                "attr"     => [
                    "id"           => "title",
                    "maxlength"    => "255",
                    "autocomplete" => "off"
                ],
                "label"    => "Url Youtube",
            ])
            ->add('input_tag', 'text', [
                "required" => true,
                "attr"     => [
                    "id"           => "tag",
                    "maxlength"    => "255",
                    "autocomplete" => "off"
                ],
                "label"    => "Tag",
            ])
            ->add('description', 'textarea', [
                "required" => true,
                "attr"     => [
                    "id"   => "description",
                    "rows" => 15
                ],
                "label"    => "Deskripsi",
            ])
            ->add("category_id", "select", [
                'choices'     => Categories::pluck("name", "id")->toArray(),
                'empty_value' => '-Pilih Rubrik-',
                'label'       => 'Rubrik',
                "required"    => true,
                "attr"        => [
                    "class" => "form-control select2"
                ]
            ])
            ->add('youtube_response', 'hidden', [
                "label" => false,
            ])
            ->add('image', 'file', [
                'attr'  => [
                    'id'       => 'file',
                    'onchange' => 'readUrl(this)',
                    'accept'   => "image/x-png,image/jpg,image/jpeg"
                ],
                'label' => 'Gambar'
            ])
            ->add('upload', 'button', [
                'label' => '<i class="fa fa-upload"></i> Browse',
                'attr'  => [
                    'id'      => 'upload-file',
                    'class'   => 'btn btn-flat bg-gray',
                    'onclick' => 'chooseFile()',
                    'style'   => 'display:none'
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<i class="fa fa-file"></i> Simpan',
                "attr"  => [
                    "id"    => "submit",
                    "class" => "btn btn-flat btn-primary btn-block"
                ]
            ]);
    }
}
