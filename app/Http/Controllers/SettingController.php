<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    protected $view = "backend.setting";
    protected $title = "Pengaturan";
    protected $url = "admin/settings";
    private $model;

    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        //$
    }
}
