<?php

namespace App\Http\Controllers;

use App\DataTables\PostDataTable;
use App\Forms\CategoryForm;
use App\Forms\PostForm;
use App\Helpers\ImageUpload;
use App\Models\Posts;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class PostsController extends Controller
{

    protected $view = "backend.posts";
    protected $title = "Video";
    protected $url = "videos";
    private $model;

    public function __construct(Posts $model)
    {
        $this->model = $model;
    }

    public function index(PostDataTable $table)
    {
        $data['title'] = $this->title;
        $data['url']   = $this->url;

        return $table->render($this->view . ".index", $data);
    }

    public function create(FormBuilder $form)
    {
        $data['title']         = $this->title;
        $data['url']           = $this->url;
        $data['form']          = $form->create(PostForm::class, [
            'method' => 'POST',
            'url'    => route("$this->url.store")
        ]);
        $data['default_video'] = "8tPnX7OPo0Q";
        $data['tags']          = $this->model->existingTags()->pluck("name");

        return view($this->view . ".form", $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'youtube_url_video' => 'bail|required|max:255|unique:posts,youtube_url_video',
            'title'             => 'required|unique:posts,title|max:255',
            'category_id'       => 'required',
            'input_tag'         => 'required',
            'description'       => 'required',
        ]);

        $input               = $request->only(['youtube_url_video', 'title', 'category_id', 'description']);
        $youtubeResponse     = (array)json_decode($request->get("youtube_response"), true);
        $input['youtube_id'] = $youtubeResponse['id'];

        $input['view_count']     = $youtubeResponse['statistics']['viewCount'];
        $input['like_count']     = $youtubeResponse['statistics']['likeCount'];
        $input['dislike_count']  = $youtubeResponse['statistics']['dislikeCount'];
        $input['comment_count']  = $youtubeResponse['statistics']['commentCount'];
        $input['published_date'] = $this->parsePublishedAt($youtubeResponse["snippet"]["publishedAt"]);
        $input['video_duration'] = $this->parseVideoDuration($youtubeResponse["contentDetails"]["duration"]);
        $input['is_slideshow']   = true;

        if ($request->hasFile("image")) {
            $input['image'] = $this->saveImageFromUpload($request);
        } else {
            $input['image'] = $this->saveImageFromYoutube($youtubeResponse["snippet"]["thumbnails"]);
        }


        try {
            \DB::beginTransaction();
            //update slideshow per category
            $this->model
                ->where("category_id", $request->get('category_id'))
                ->where("is_slideshow", true)
                ->update(["is_slideshow" => false]);

            $post = $this->model->create($input);
            $post->tag(explode(",", $request->get("input_tag")));


            \DB::commit();
            flash($this->title . " berhasil dibuat.")->success();

        } catch (\Exception $e) {
            \Log::info($e);
            \DB::rollBack();

            flash($this->title . " gagal dibuat.")->error();
        }

        return redirect()->route($this->url . ".index");
    }

    public function show($id)
    {
        //
    }

    public function edit(FormBuilder $form, $id)
    {
        try {
            $row = $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route($this->url . ".index");
        }

        $data['title']         = $this->title;
        $data['url']           = $this->url;
        $data['row']           = $row;
        $data['tags']          = $row->existingTags()->pluck("name")->toArray();
        $data['default_video'] = $row->youtube_id;

        $data['form'] = $form->create(PostForm::class, [
            'method' => 'PATCH',
            'url'    => route("$this->url.update", $id),
            'model'  => $row
        ])->modify("input_tag", 'text', [
            "value" => implode(",", $row->tag_names)
        ])->modify("youtube_url_video", "text", [
            "attr" => [
                "readonly" => true
            ]
        ]);

        return view($this->view . ".form", $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'youtube_url_video' => "bail|required|max:255|unique:posts,youtube_url_video," . $id . ",id,deleted_at,NULL",
            'title'             => "required|unique:posts,title," . $id . ",id,deleted_at,NULL|max:255",
            'category_id'       => 'required',
            'input_tag'         => 'required',
            'description'       => 'required',
        ]);

        $input = $request->only(['youtube_url_video', 'title', 'category_id', 'description']);

        $post = $this->model->find($id);
        $post->update($input);
        $post->retag(explode(",", $request->get("input_tag")));

        //save tags
        flash($this->title . " berhasil diubah.")->success();

        return redirect()->route($this->url . ".index");
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        return $this->title . " berhasil dihapus.";
    }

    public function fetchYoutubeVideo(Request $request)
    {
        try {
            $videoId = \Youtube::parseVIdFromURL($request->get("url"));
            $video   = \Youtube::getVideoInfo($videoId);
            if ( ! $video) {
                return response()->json('Video tidak valid', 400);
            }

            return response()->json($video);
        } catch (\Exception $e) {
            \Log::error($e);

            return response()->json($e->getMessage(), 400);
        }
    }

    private function saveImageFromYoutube($thumbnails)
    {
        return (new ImageUpload())->uploadFromYoutube($thumbnails);
    }

    private function parsePublishedAt($publishedAt)
    {
        return Carbon::parse($publishedAt)->timezone(config("app.timezone"))->toDateTimeString();
    }

    private function parseVideoDuration($duration)
    {
        $start = new DateTime('@0');
        $start->add(new DateInterval($duration));

        return $start->format('H:i:s');
    }

    private function saveImageFromUpload(Request $request)
    {
        return (new ImageUpload($request))->upload("post");
    }

    public function optimizeImage(Request $request)
    {
        $process = (new ImageUpload())->tinifyCompress($request->get("image"));

        return $process ? response()->json("sukses") : response()->json("error", 500);
    }

}
