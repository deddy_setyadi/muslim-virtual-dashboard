<?php

namespace App\Http\Controllers;

use App\DataTables\CategoryDataTable;
use App\Forms\CategoryForm;
use App\Helpers\ImageUpload;
use App\Models\Categories;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class CategoryController extends Controller
{

    protected $view = "backend.category";
    protected $url = "categories";
    protected $title = "Rubrik";
    private $model;

    public function __construct(Categories $model)
    {
        $this->model = $model;
    }

    public function index(CategoryDataTable $table)
    {
        $data['title'] = $this->title;
        $data['url']   = $this->url;

        return $table->render($this->view . ".index", $data);
    }

    public function create(FormBuilder $form)
    {
        $data['title'] = $this->title;
        $data['url']   = $this->url;
        $data['form']  = $form->create(CategoryForm::class, [
            'method' => 'POST',
            'url'    => route("$this->url.store")
        ]);

        return view($this->view . ".form", $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'bail|required|max:255|unique:categories,name',
            'description' => 'required|max:255',
        ]);

        $input = $request->only(['name', 'description']);

        if ($request->hasFile("image")) {
            $photo          = (new ImageUpload($request))->upload();
            $input['image'] = $photo;
        }

        $this->model->create($input);

        flash($this->title . " berhasil dibuat.")->success();

        return redirect()->route($this->url . ".index");
    }

    public function show($id)
    {
        //
    }

    public function edit(FormBuilder $form, $id)
    {
        $row = $this->model->find($id);

        $data['title'] = $this->title;
        $data['url']   = $this->url;
        $data['row']   = $row;
        $data['form']  = $form->create(CategoryForm::class, [
            'method' => 'PATCH',
            'url'    => route("$this->url.update", $id),
            'model'  => $row
        ])->modify('image', 'file', [
            'attr'     => [
                'id'       => 'file',
                'onchange' => 'readUrl(this)',
                'accept'   => "image/x-png,image/jpg,image/jpeg"
            ],
            'label'    => 'Gambar',
            'required' => false
        ]);

        return view($this->view . ".form", $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'bail|required|max:255|unique:categories,name,' . $id . ",id,deleted_at,NULL",
            'description' => 'required|max:255',
        ]);

        $input = $request->only(['name', 'description']);
        $row   = $this->model->find($id);

        if ($request->hasFile("image")) {
            $uploader = new ImageUpload($request);
            $uploader->delete($row->image);

            $photo          = $uploader->upload();
            $input['image'] = $photo;
        }

        $row->update($input);

        flash($this->title . " berhasil diubah.")->success();

        return redirect()->route($this->url . ".index");
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        return $this->title . " berhasil dihapus.";
    }
}
