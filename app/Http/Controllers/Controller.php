<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Posts;
use App\Models\Tag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getCategories()
    {
        return Categories::oldest("name")->get();
    }

    protected function getTags($limit = 12)
    {
        return Tag::latest("count")->limit($limit)->get();
    }

    protected function getMostViewedVideo($limit = 8)
    {
        return Posts::latest("view_count")->limit($limit)->get();
    }

    protected function getMostLikedVideo($limit = 8, $except = null)
    {
        if ( ! is_null($except)) {
            return Posts::where("id", "!=", $except)->latest("like_count")->limit($limit)->get();
        }

        return Posts::latest("like_count")->limit($limit)->get();
    }

    public function getGoogleAnalyticCode()
    {
        return config("app.google.analytic");
    }
}
