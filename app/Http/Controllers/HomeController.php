<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Posts;
use App\Models\Tagged;
use Artesaos\SEOTools\Traits\SEOTools;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    use SEOTools;

    private $posts;
    private $categories;
    private $tagged;

    public function __construct(Posts $posts, Categories $categories, Tagged $tagged)
    {
        $this->posts      = $posts;
        $this->categories = $categories;
        $this->tagged     = $tagged;
    }


    public function index()
    {
        $this->getDefaultSEO();

        $featured = $this->posts
            ->withoutSlideShow()
            ->with("category")
            ->limit(17)
            ->latest("published_date")
            ->get();

        $slides = $this->posts->slideShow()->with("category")->inRandomOrder()->get();

        $firstFeatured      = $featured->first();
        $firstListFeatured  = $featured->slice(1, 8);
        $secondListFeatured = $featured->slice(9, 8);
        $mostViewed         = $this->getMostViewedVideo();
        $mostLiked          = $this->getMostLikedVideo();

        $categories = $this->getCategories();

        return view('frontend.home')
            ->with("first_featured", $firstFeatured)
            ->with("first_list_featured", $firstListFeatured)
            ->with("second_list_featured", $secondListFeatured)
            ->with("most_viewed", $mostViewed)
            ->with("most_liked", $mostLiked)
            ->with("categories", $categories)
            ->with("analyticCode", $this->getGoogleAnalyticCode())
            ->with("slides", $slides);
    }

    public function singlePost($videoSlug)
    {
        $categories = $this->getCategories();
        $post       = $this->posts->where("slug", $videoSlug)->with("category", "tagged")->firstOrFail();

        $this->seo()
             ->metatags()
             ->setTitle($post->title)
             ->setDescription(config("app.description"))
             ->addMeta('article:published_time', $post->published_date->toW3CString(), 'property')
             ->addMeta('article:section', $post->category->name, 'property')
             ->addKeyword($post->tagNames())
             ->setCanonical(url("/show/" . $post->slug))
             ->addMeta("robots", "index,follow");

        $take_image = take_image($post->image, 'standard');
        $this->seo()
             ->opengraph()
             ->setTitle($post->title)
             ->setDescription($post->description)
             ->setUrl(url("/show/" . $post->slug))
             ->addProperty('type', 'article')
             ->setSiteName('muslimvirtual')
             ->addImages([
                 $take_image,
                 [
                     "type"   => \Image::make($take_image)->mime(),
                     "width"  => "640",
                     "height" => "480"
                 ]
             ]);

        $this->seo()->twitter()
             ->setTitle($post->title)
             ->setDescription($post->description)
             ->setUrl(url("/show/" . $post->slug))
             ->setSite(config("app.name"));

        $relatedPost = $this->posts
            ->where("category_id", $post->category->id)
            ->where("id", "!=", $post->id)
            ->limit(3)
            ->get();

        $mostLikedVideo = $this->getMostLikedVideo(5, $post->id);

        return view('frontend.single-post')
            ->with("categories", $categories)
            ->with("post", $post)
            ->with("analyticCode", $this->getGoogleAnalyticCode())
            ->with("related_post", $relatedPost)
            ->with("most_liked", $mostLikedVideo);
    }

    public function category($categorySlug)
    {
        $this->getDefaultSEO();

        $categories = $this->getCategories();

        $category = $this->categories->where("slug", $categorySlug)->firstOrFail();
        $posts    = $this->posts
            ->where("category_id", $category->id)
            ->with("category")
            ->latest("published_date")
            ->paginate(5);

        $mostLikedVideo = $this->getMostLikedVideo(8);

        return view('frontend.category')
            ->with("categories", $categories)
            ->with("category", $category)
            ->with("posts", $posts)
            ->with("analyticCode", $this->getGoogleAnalyticCode())
            ->with("most_liked", $mostLikedVideo);
    }

    public function tag($tagSlug)
    {
        $this->getDefaultSEO();

        $categories = $this->getCategories();

        $posts = $this->posts->withAnyTag([$tagSlug])->with("category")->latest("published_date")->paginate(5);
        $tag   = $this->tagged->where("tag_slug", $tagSlug)->firstOrFail();

        $mostLikedVideo = $this->getMostLikedVideo(8);

        return view('frontend.tag')
            ->with("categories", $categories)
            ->with("tag", $tag)
            ->with("posts", $posts)
            ->with("analyticCode", $this->getGoogleAnalyticCode())
            ->with("most_liked", $mostLikedVideo);
    }

    public function search(Request $request)
    {
        $this->getDefaultSEO();

        $categories = $this->getCategories();

        $posts = $this->posts
            ->where(\DB::raw("LOWER(title)"), "like", "%" . strtolower($request->get("search")) . "%")
            ->where(\DB::raw("LOWER(description)"), "like", "%" . strtolower($request->get("search")) . "%")
            ->with("category")
            ->latest("published_date")
            ->paginate(5);

        $mostLikedVideo = $this->getMostLikedVideo(8);

        return view('frontend.search')
            ->with("categories", $categories)
            ->with("keyword", $request->get("search"))
            ->with("posts", $posts)
            ->with("analyticCode", $this->getGoogleAnalyticCode())
            ->with("most_liked", $mostLikedVideo);
    }

    public function about()
    {
        $this->getDefaultSEO();

        $categories     = $this->getCategories();
        $mostLikedVideo = $this->getMostLikedVideo(5);

        return view('frontend.about')->with("categories", $categories)->with("most_liked", $mostLikedVideo);
    }

    public function contact()
    {
        $this->getDefaultSEO();

        $categories     = $this->getCategories();
        $mostLikedVideo = $this->getMostLikedVideo(5);

        return view('frontend.contact')->with("categories", $categories)->with("most_liked", $mostLikedVideo);
    }

    public function sitemap()
    {
        $sitemap = \App::make('sitemap');

        // add item to the sitemap (url, date, priority, freq)
        $sitemap->add(url('/'), Carbon::yesterday()->toW3cString(), '1.0', 'daily');
        foreach ($this->getCategories() as $category) {
            $sitemap->add(url('category/' . $category->slug), Carbon::yesterday()->toW3cString(), '0.9', 'daily');
        }

        // get all posts from db
        $posts = $this->posts->orderBy('created_at', 'desc')->get(['slug', 'title', 'created_at', 'image']);

        // add every post to the sitemap
        foreach ($posts as $post) {
            //$sitemap->add($post->slug, $post->modified, $post->priority, $post->freq);
            $images = [['url' => take_image($post->image, "medium")],];
            $sitemap->add(url("show/" . $post->slug), $post->created_at->toW3cString(), '1.0', 'monthly', $images);
        }

        return $sitemap->render('xml');
    }

    public function feed()
    {
        $feed = \App::make("feed");

        $feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'

        $posts = $this->posts->orderBy('created_at', 'desc')->take(20)->get();

        // set your feed's title, description, link, pubdate and language
        $feed->title       = config("app.name");
        $feed->description = config("app.description");
        $feed->logo        = url("img/logo/logo-putih-ijo.png");
        $feed->link        = url('feed');
        $feed->pubdate     = $posts->first()->created_at;
        $feed->lang        = 'id';
        $feed->setShortening(true); // true or false
        $feed->setTextLimit(100); // maximum length of description text

        foreach ($posts as $post) {
            // set item's title, author, url, pubdate, description, content, enclosure (optional)*
            $feed->add(
                $post->title,
                "Muslim Virtual",
                url("show/" . $post->slug),
                $post->created_at,
                $post->description,
                $post->youtube_url_video
            );
        }

        return $feed->render('atom');
    }

    private function getDefaultSEO()
    {
        $this->seo()->metatags()
             ->setTitle(config("app.name"))
             ->setDescription(config("app.description"))
             ->addKeyword([
                 "muslim virtual",
                 "Fiqih Virtual",
                 "milenial",
                 "Islam Virtual",
                 "Tokoh",
                 "Ulama",
                 "Literasi Virtual",
                 "Pesantren"
             ])
             ->setCanonical(url()->current())
             ->addMeta("robots", "index,follow");

        $this->seo()->opengraph()
             ->setTitle(config("app.name"))
             ->setDescription(config("app.description"))
             ->setUrl(url()->current())
             ->addProperty('type', 'article')
             ->setSiteName(config("app.url"))
             ->addImages([
                 "url"    => config("app.url") . "/img/logo/logo-mv.jpg",
                 "type"   => "image/jpeg",
                 "width"  => "600",
                 "height" => "400"
             ]);

        $this->seo()->twitter()
             ->setTitle(config("app.name"))
             ->setDescription(config("app.description"))
             ->setUrl(url()->current())
             ->setSite(config("app.url"));

        return $this;
    }

    public function mediaGuide()
    {
        $this->getDefaultSEO();

        $categories     = $this->getCategories();
        $mostLikedVideo = $this->getMostLikedVideo(5);

        return view('frontend.media-guide')
            ->with("categories", $categories)
            ->with("most_liked", $mostLikedVideo);
    }
}

