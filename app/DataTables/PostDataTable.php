<?php

namespace App\DataTables;

use App\Models\Posts;
use Yajra\DataTables\Services\DataTable;

class PostDataTable extends DataTable
{

    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn("title", function ($row) {
                return "<h4><b>" . \Str::limit($row->title, 60) . "</b></h4>"
                       . "<p>" . \Str::limit($row->description, 80) . "</p>"
                       . "<p><i class='fa fa-pencil'></i> " . $row->created_at->format('d/m/Y H:i')
                       . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-upload'></i> " . $row->published_date->format("d/m/Y H:i")
                       . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-clock-o'></i> " . $row->video_duration ."</p>"
                       ."<p class='text-bold'><i class='fa fa-th-large'></i> " . $row->category->name . "</p>";
            })
            ->editColumn("image", function ($row) {
                return "<img src='" . take_image($row->image) . "' style=\"max-height:120px\" class=\"lazy-image thumbnail\">";
            })
            ->editColumn("view_count", function ($row) {
                return "<p class='text-info text-bold'><i class='fa fa-eye'></i> " . format_nominal($row->view_count) . "</p>"
                       . "<p class='text-success text-bold'><i class='fa fa-thumbs-o-up'></i> " . format_nominal($row->like_count) . "</p>"
                       . "<p class='text-danger text-bold'><i class='fa fa-thumbs-o-down'></i> " . format_nominal($row->dislike_count) . "</p>"
                       . "<p class='text-purple text-bold'><i class='fa fa-comments-o'></i> " . format_nominal($row->comment_count) . "</p>";
            })
            ->addColumn("action", function ($row) {
                return "<div class=\"btn-group-vertical\" role=\"group\" aria-label=\"...\">
                <a href='" . $row->youtube_url_video . "' target='_blank' class='btn btn-flat btn-default'><i class='fa fa-youtube-play'></i> Youtube </a>
                <button onclick=\"optimizeImage('".$row->image."')\" class='btn btn-flat btn-warning' data-toggle='modal' data-target='modal-resize-image'>
                    <i class='fa fa-circle-o-notch'></i> Opt. Image
                </button>
                 <a href='" . route("videos.edit", $row->id) . "' class='btn btn-flat btn-success' data-toggle='tooltip' data-original-title='Edit'>
					<i class='fa fa-pencil'></i> Edit
				</a>
				<button href='" . route("videos.destroy", $row->id) . "' class='btn btn-flat btn-danger' data-toggle='tooltip' data-original-title='Delete'  onclick='swal_alert_delete(this); return false;'>
					<i class='fa fa-trash-o'></i> Hapus
				</button>
                </div>";
            })
            ->editColumn("created_at", function ($row) {
                $result = "<p> <i class='fa fa-plus-circle'></i> " . $row->created_at->format("d-m-Y H:i") . "</p>" .
                          "<p><i class='fa fa-pencil-square-o'></i> " . $row->updated_at->format("d-m-Y H:i") . "</p>";

                return $result;
            })
            ->rawColumns(['image', 'title', 'action', 'view_count']);
    }

    public function query(Posts $model)
    {
        return $model
            ->newQuery()
            ->select('id', 'title', 'description', 'image', 'slug', 'youtube_url_video', 'published_date',
                'is_published', 'category_id', 'created_at', 'updated_at', 'view_count', 'like_count', 'dislike_count',
                'comment_count', 'video_duration')
            ->with([
                "category" => function ($q) {
                    return $q->select("id", "name");
                }
            ])
            ->latest("published_date");
    }

    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '10%'])
                    ->parameters([
                        'dom'           => '<"tableHeader"<"row"<"col-md-2"f><"col-md-10"p>>><"newProcessing"r>t<"tableFooter"<"row"<"col-md-4"l><"col-md-4"i><"col-md-4"p>>>',
                        'bSort'         => true,
                        "bLengthChange" => true,
                        "bAutoWidth"    => true,
                        "bInfo"         => true
                    ]);
    }

    protected function getColumns()
    {
        return [
            'id'           => [
                'visible' => false
            ],
            'image'        => [
                'title' => 'Gambar'
            ],
            'title'        => [
                'title' => 'Judul',
                'width' => "50%"
            ],
            'view_count'   => [
                'title' => 'Statistik Youtube',
                'width' => '20%'
            ],
            'description'  => [
                'visible' => false
            ],
            'slug'         => [
                'visible' => false
            ],
            'is_published' => [
                'visible' => false
            ],
            'created_at'   => [
                'visible' => false
            ],
            'updated_at'   => [
                'visible' => false
            ],
        ];
    }
}
