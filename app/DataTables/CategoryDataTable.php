<?php

namespace App\DataTables;

use App\Models\Categories;
use Yajra\DataTables\Services\DataTable;

class CategoryDataTable extends DataTable
{

    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn("name", function ($row) {
                return "<h5><b>" . $row->name . "</b></h5>" . "<p>" . $row->description . "</p>";
            })
            ->editColumn("image", function ($row) {
                return "<img src='" . take_image($row->image) . "' style=\"max-height:100px\" class=\"lazy-image thumbnail\">";
            })
            ->addColumn("action", function ($row) {
                return "<div class=\"btn-group-vertical\" role=\"group\" aria-label=\"...\">
                 <a href='" . route("categories.edit", $row->id) . "' class='btn btn-flat btn-success' data-toggle='tooltip' data-original-title='Edit'>
					<i class='fa fa-pencil'></i> Edit
				</a>
				<button href='" . route("categories.destroy", $row->id) . "' class='btn btn-flat btn-danger' data-toggle='tooltip' data-original-title='Delete'  onclick='swal_alert_delete(this); return false;'>
					<i class='fa fa-trash-o'></i> Hapus
				</button>
                </div>";
            })
            ->editColumn("created_at", function ($row) {
                return "<p> <i class='fa fa-plus-circle'></i> " . $row->created_at->format("d-m-Y H:i") . "</p>" .
                       "<p><i class='fa fa-pencil-square-o'></i> " . $row->updated_at->format("d-m-Y H:i") . "</p>";
            })
            ->rawColumns(['image', 'name', 'created_at', 'action']);
    }

    public function query(Categories $model)
    {
        return $model
            ->newQuery()
            ->select('id', 'name', 'description', 'image', 'slug', 'created_at', 'updated_at')
            ->latest();
    }

    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '10%'])
                    ->parameters([
                        'dom'           => '<"tableHeader"<"row"<"col-md-2"f><"col-md-10"p>>><"newProcessing"r>t<"tableFooter"<"row"<"col-md-4"l><"col-md-4"i><"col-md-4"p>>>',
                        'bSort'         => true,
                        "bLengthChange" => true,
                        "bAutoWidth"    => true,
                        "bInfo"         => true
                    ]);
    }

    protected function getColumns()
    {
        return [
            'id'           => [
                'visible' => false
            ],
            'image'        => [
                'title' => 'Gambar'
            ],
            'name'         => [
                'title' => 'Nama Rubrik',
            ],
            'description'  => [
                'visible' => false
            ],
            'slug'         => [
                'visible' => false
            ],
            'created_at'   => [
                'title' => 'Tanggal',
                'width' => '20%'
            ],
            'updated_at'   => [
                'visible' => false
            ],
        ];
    }
}
