<?php

use Carbon\Carbon;

if ( ! function_exists('take_image')) {
    /**
     * Helper to get image file.
     *
     * @param $imageUrl
     * @param string $size
     * @param bool $user
     *
     * @return string
     */
    function take_image($imageUrl, $size = "medium", $user = false)
    {
        $imgPath = "img/assets";

        if ( ! empty($imageUrl) and ! is_null($imageUrl) and $imageUrl != "") {
            $imagePath = $imgPath . "/" . $size . "/" . $imageUrl;
            if (file_exists(public_path($imagePath))) {
                $imgUrl = $imgPath . "/" . $size . "/" . $imageUrl;

                return url($imgUrl);
            }
        }

        if ($user == true) {
            return url("img/assets/noimageuser.png");
        }

        return url("img/assets/noimage.png");
    }
}

if ( ! function_exists('list_month_id')) {
    /**
     * Helper to get list of month.
     *
     *
     * @return array
     */
    function list_month_id()
    {

        return [
            1  => 'Januari',
            2  => 'Februari',
            3  => 'Maret',
            4  => 'April',
            5  => 'Mei',
            6  => 'Juni',
            7  => 'Juli',
            8  => 'Agustus',
            9  => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        ];
    }
}

if ( ! function_exists('list_day_id')) {
    /**
     * Helper to get list of month.
     *
     *
     * @return array
     */
    function list_day_id()
    {
        return [
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu',
        ];
    }
}
if ( ! function_exists('format_nominal')) {
    /**
     * Helper to make format idr for money.
     *
     * @param $number
     * @param int $prefix
     *
     * @return string
     */
    function format_nominal($number, $prefix = 0)
    {
        return ! is_null($number) ? number_format($number, $prefix, ",", ".") : 0;
    }
}

if ( ! function_exists('date_frontend')) {
    /**
     * Helper to make format idr for money.
     *
     * @param $date
     * @param bool $time
     *
     * @return string
     */
    function date_frontend($date, $time = false)
    {
        $date = Carbon::parse($date);

        return ! $time
            ? $date->day . " " . list_month_id()[$date->month] . " " . $date->year
            : $date->day . " " . list_month_id()[$date->month] . " " . $date->year . " " . $date->hour . ":" . $date->minute;
    }
}

if ( ! function_exists('count_video_in_category')) {
    /**
     * Helper to make format idr for money.
     *
     * @param $categoryId
     *
     * @return string
     */
    function count_video_in_category($categoryId)
    {
        return \App\Models\Posts::where("category_id", $categoryId)->count();
    }
}

if ( ! function_exists('numberformat_kilo')) {
    /**
     * Helper to make format idr for money.
     *
     * @param $date
     * @param bool $time
     *
     * @return string
     */
    function numberformat_kilo($number)
    {
        if ( ! is_numeric($number)) {
            return 0;
        }

        if ($number < 1) {
            return 0;
        } else if ($number < 1000) {
            return $number;
        }

        $prefix  = ['', "rb", "jt", "m", "t"];
        $divider = [1, 1000, 1000000, 1000000000, 1000000000000];

        // proses
        $factor = floor((strlen($number) - 1) / 3);
        $suffix = $prefix[$factor];
        $divide = $divider[($factor)];
        $result = number_format($number / $divide, 2, ',', '');

        echo $result . " $suffix";
    }
}

if ( ! function_exists('remove_non_utf8')) {
    /**
     * Helper to make format idr for money.
     *
     * @param $string
     *
     * @return string
     */
    function remove_non_utf8($string)
    {
        return preg_replace('/(?:^[^\p{L}\p{N}]+|[^\p{L}\p{N}]+$)/u', '', $string);
    }
}