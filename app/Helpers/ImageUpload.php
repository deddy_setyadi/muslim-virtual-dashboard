<?php

namespace App\Helpers;

use Tinify\AccountException;
use function Tinify\fromBuffer;
use function Tinify\fromFile;
use function Tinify\setKey;

class ImageUpload
{

    public $request;
    public $width;
    public $height;
    public $folder;
    public $result;
    protected $name;
    protected $picInfo;

    public function __construct($request = null)
    {
        $this->request = $request;
        $this->folder  = "img/assets";
        $this->picInfo = [
            ["width" => 120, "height" => 90, "path" => "default"],
            ["width" => 320, "height" => 180, "path" => "medium"],
            ["width" => 480, "height" => 360, "path" => "high"],
            ["width" => 640, "height" => 480, "path" => "standard"],
            ["width" => 1280, "height" => 720, "path" => "maxres"],
        ];
    }

    /*
    * Fungsi Utama
     */
    public function upload($type = "default")
    {
        if ( ! is_null($this->request["image"])) {
            if ($type == "post") {
                $this->checkFolder()->nameFile()->uploadImagePost();
            } else {
                $this->checkFolder()->nameFile()->uploadImage();
            }

            return $this->result;
        }

        return null;
    }

    public function uploadFromYoutube($images)
    {
        $this->checkFolder()->nameFile()->uploadYoutube($images);

        return $this->result;
    }

    public function delete($imgName)
    {
        if ( ! empty($imgName) and ! is_null($imgName) and $imgName != "") {
            foreach ($this->picInfo as $item) {
                \File::delete($this->folder . "/" . $item['path'] . "/" . $imgName);
            }
        }
    }

    protected function checkFolder()
    {
        $folder = public_path() . "/" . $this->folder;
        if ( ! file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        return $this;
    }

    protected function nameFile()
    {
        $datetime  = intval(microtime(true) * 10000000 + rand(10000, 20000000));
        $extension = ( ! is_null($this->request)) ? $this->request['image']->getClientOriginalExtension() : "jpg";

        $this->name = $datetime . "." . $extension;

        return $this;
    }

    /*
     * Create maxres, default, dtandard, high, medium image and return name as string
     * @return String
     * */
    protected function uploadImage()
    {
        foreach ($this->picInfo as $item) {
            $file = \Image::make($this->request['image'])
                          ->resize($item['width'], $item['height'], function ($c) {
                              $c->aspectRatio();
                              $c->upsize();
                          });

            $file->save($this->folder . "/" . $item['path'] . "/" . $this->name, 60);
        }

        $this->result = $this->name;

        return $this;
    }

    private function uploadYoutube($images)
    {
        foreach ($this->picInfo as $item) {
            $file = \Image::make($images[$item['path']]['url']);
            $file->save($this->folder . "/" . $item['path'] . "/" . $this->name);
        }

        $this->result = $this->name;

        return $this;
    }

    private function uploadImagePost()
    {
        foreach ($this->picInfo as $item) {
            $file = \Image::make($this->request['image'])
                          ->resize($item['width'], $item['height'], function ($c) {
                              $c->aspectRatio();
                              $c->upsize();
                          });
            $file->resizeCanvas($item['width'], $item['height'], 'center', false, '000000');

            $file->save($this->folder . "/" . $item['path'] . "/" . $this->name, 60);
        }
        $this->tinifyCompress($this->name);

        $this->result = $this->name;

        return $this;
    }

    /**
     * @param $image
     *
     * @return bool
     */
    public function tinifyCompress($image)
    {
        setKey(env("TINYFY_API_KEY"));
        foreach ($this->picInfo as $item) {
            try {
                $path       = $this->folder . "/" . $item['path'] . "/" . $image;
                $sourceData = fromFile($path);
                $sourceData->toFile($this->folder . "/" . $item['path'] . "/" . $image);

            } catch (\Exception $e) {
                \Log::error("The error message is: " . $e->getMessage());

                return false;
            }
        }

        return true;
    }
}
