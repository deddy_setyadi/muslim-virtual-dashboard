<?php

namespace App\Console\Commands;

use App\Models\Posts;
use Illuminate\Console\Command;

class RefreshVideo extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh video watcher, like and comment from youtube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $posts = Posts::all();
        if ($posts->count() == 0) {
            return false;
        }

        foreach ($posts as $post) {
            $fetchYoutube = \Youtube::getVideoInfo($post->youtube_id);
            if ( ! $fetchYoutube) {
                exit("error retrieving youtube information");
            }

            $input['view_count']    = $fetchYoutube->statistics->viewCount;
            $input['like_count']    = $fetchYoutube->statistics->likeCount;
            $input['dislike_count'] = $fetchYoutube->statistics->dislikeCount;
            $input['comment_count'] = $fetchYoutube->statistics->commentCount;

            $post->update($input);
            sleep(3);
        }

        \Log::info("Successfully update " . $posts->count() . " video");
    }
}
