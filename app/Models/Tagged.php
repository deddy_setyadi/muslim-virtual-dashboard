<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class Tagged extends \Conner\Tagging\Model\Tagged
{

    use Cachable;
}
