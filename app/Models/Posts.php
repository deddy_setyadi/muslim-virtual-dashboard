<?php

namespace App\Models;

use Conner\Tagging\Taggable;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{

    use SoftDeletes, Cachable, Sluggable, Taggable, Sharable;
    protected $table = "posts";
    protected $dates = ["published_date"];
    protected $guarded = ["id"];
    protected $shareOptions = [
        'columns' => [
            'title' => 'title'
        ],
        'url'     => 'url'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }

    public function scopeWithoutSlideShow($query)
    {
        return $query->where("is_slideshow", 0);
    }

    public function scopeSlideShow($query)
    {
        return $query->where("is_slideshow", 1);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = remove_non_utf8($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = remove_non_utf8($value);
    }

    public function getUrlAttribute()
    {
        return url("show/" . $this->slug);
    }
}
