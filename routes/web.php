<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/show/{videoSlug}', 'HomeController@singlePost');
Route::get('/category/{categorySlug}', 'HomeController@category');
Route::get('/tag/{tagSlug}', 'HomeController@tag');
Route::get('/search', 'HomeController@search');
Route::get('/about-us', 'HomeController@about');
Route::get('/media-guide', 'HomeController@mediaGuide');
Route::get('/contact', 'HomeController@contact');
Route::get('/sitemap', 'HomeController@sitemap');
Route::get('/feed', 'HomeController@feed');

Auth::routes(["register" => false]);

Route::middleware("auth")->prefix("admin")->group(function () {
    Route::get("/", "DashboardController@index");

    Route::post("/videos/fetch-youtube", "PostsController@fetchYoutubeVideo");
    Route::post("/videos/optimize-image", "PostsController@optimizeImage");
    Route::resource("/videos", "PostsController")->except(["show"]);
    Route::resource("/categories", "CategoryController")->except(["show"]);

    Route::get("clear-cache", function () {
        return Artisan::call("cache:clear");
    });

    Route::get("clear-view", function () {
        return Artisan::call("view:clear");
    });

    Route::get("migrate", function () {
        return Artisan::call("migrate");
    });
});
